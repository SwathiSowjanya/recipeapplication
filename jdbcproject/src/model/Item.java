package model;

public class Item {
	private Integer id;
	private String name;
	private String type;
	private String ingredients;

	public Item() {

	}

	public Item(Integer id, String name, String type, String ingredients) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.ingredients = ingredients;
	}

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIngredients() {
		return ingredients;
	}

	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public String toString() {
		return String.format("%-20s%-20s%-20s%-20s", id, name,type,ingredients);
	}

}