package dao;

//import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Item;
import util.Db;
import util.Query;

public class ItemImpl implements IItem {
	PreparedStatement statement;
	String result;

	@Override
	public String addItem(Item item) {
		try {
			statement = Db.getObject().getConnection().prepareStatement(Query.ADD_ITEM);
			statement.setInt(1, item.getId());
			statement.setString(2, item.getName());
			statement.setString(3, item.getType());
			statement.setString(4, item.getIngredients());
			statement.executeUpdate();
			result="Inserted Successfully";
		} catch ( SQLException ex) {
			result = "Duplicate Exists";
		}
		return result;
	}

	@Override
	public List<Item> viewItem() {
		List<Item> list=new ArrayList<Item>();
		try {
			statement=Db.getObject().getConnection().prepareStatement(Query.VIEW_ALL_ITEM);
		  ResultSet set= statement.executeQuery();		
		  while(set.next()) {
			  Item item=new Item(set.getInt(1),set.getString(2),set.getString(3),set.getString(4));
			  list.add(item);
		  }
		}catch( SQLException ex) {
			System.err.println(ex.getMessage());
		}
		return list;
	}

	@Override
	public String updateItem(Item item) {
		try {
			 statement=Db.getObject().getConnection().prepareStatement(Query.UPDATE_ITEM);
			 statement.setInt(4,item.getId());
			 statement.setString(1,item.getName());
			 statement.setString(2, item.getType());
				statement.setString(3, item.getIngredients());
			 int status=statement.executeUpdate();
			 if (status>0) {
				 result="Update Sucessfully";
			 }else {
				 result="Record Not Found";
			 }
		}catch( SQLException ex) {
			System.err.println(ex.getMessage());
		}
		return result;
		
	}

	@Override
	public String deleteItem(Item item) {
		try {
			 statement=Db.getObject().getConnection().prepareStatement(Query.DELETE_ITEM);
			 statement.setInt(1,item.getId());
			 int status=statement.executeUpdate();
			 if (status>0) {
				 result="Deleted Sucessfully";
			 }else {
				 result="Record Not Found";
			 }
		}catch( SQLException ex) {
			System.err.println(ex.getMessage());
		}
		return result;
		
	}

}
