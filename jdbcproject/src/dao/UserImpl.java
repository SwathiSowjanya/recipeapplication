package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;
import util.Db;
import util.Query;

public class UserImpl implements IUser {
	PreparedStatement statement;
	String result;

	@Override
	public String userLogin(User user) {
		try {
			
			statement = Db.getObject().getConnection().prepareStatement(Query.USER_LOGIN);
			statement.setString(1, user.getUsername());
			statement.setString(2, user.getPassword());
			ResultSet rs = statement.executeQuery();
			int temp = 0;
			while (rs.next()) {
				temp++;
			}
			if (temp > 0) {
				result = "Welcome To UserPage";
			} else {
				System.out.println("Username or password is Incorrect");
				System.exit(0);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public String adduser(User user) {
		try {
			statement  = Db.getObject().getConnection().prepareStatement(Query.REGISTER_USER);
			statement.setString(1, user.getUsername());
			statement.setString(2, user.getPassword());
			int status = statement.executeUpdate();
			
			if (status > 0) {
				result ="Registered Successfully";
			}else {
				result="Please try again";
			}
		} catch (SQLException ex) {
			System.err.println(ex.getMessage());
		}

		return result;
	}

}
