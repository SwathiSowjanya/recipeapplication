package dao;

import java.util.List;

import model.Item;

public interface IItem {
	public String addItem(Item item);

	public List<Item> viewItem();

	public String updateItem(Item item);

	public String deleteItem(Item item);

}
