package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Admin;
import util.Db;
import util.Query;

public class AdminImpl implements IAdmin {

	@Override
	public String AdminLogin(Admin admin) {
		try {
			PreparedStatement prepare = Db.getObject().getConnection().prepareStatement(Query.ADMIN_LOGIN);
			prepare.setString(1, admin.getUsername());
			prepare.setString(2, admin.getPassword());
			int i = 0;
			ResultSet rs = prepare.executeQuery();
			while (rs.next()) {
				i++;
			}
			if (i > 0) {
				return "Successfully";

			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return "failed";
	}

}
