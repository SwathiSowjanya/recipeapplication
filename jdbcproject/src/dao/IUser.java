package dao;


import model.User;

public interface IUser {

	public String userLogin(User user);

	public String adduser(User user);
}
