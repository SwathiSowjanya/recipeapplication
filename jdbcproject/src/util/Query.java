package util;

public class Query {
	public static final String ADD_ITEM = "insert into item values(?,?,?,?)";
	public static final String DELETE_ITEM = "delete from item where id=?";
	public static final String UPDATE_ITEM = "update item set name=?,type=?,ingredients=? where id=?";
	public static final String VIEW_ALL_ITEM = "select * from item";

	public static final String ADMIN_LOGIN = "select * from admin where username=? and password=?";
	public static final String USER_LOGIN = "select * from user where username1=? and password1=?";
	public static final String REGISTER_USER = "insert into user values(?,?)";

}
