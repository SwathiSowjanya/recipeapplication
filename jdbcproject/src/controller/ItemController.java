package controller;

import java.util.List;

import dao.ItemImpl;
import model.Item;

public class ItemController {
	Item item;
	ItemImpl impl = new ItemImpl();

	public String addItem(Integer id, String name ,String type, String ingredients) {
		item = new Item(id, name, type ,ingredients);
		return impl.addItem(item);
	}
	public List<Item>viewItem(){
		return impl.viewItem();

}
	public String updateItem(Integer id, String name ,String type, String ingredients) {
		item = new Item(id, name,type,ingredients);
		return impl.updateItem(item);
		
	}
	public String deleteItem(Integer id) {
		item=new Item();
		item.setId(id);
		return impl. deleteItem(item);
	}
}