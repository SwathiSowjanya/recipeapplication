package controller;

import dao.IUser;
import dao.UserImpl;
import model.User;

public class UserController {
	 static User user;
     static IUser impl = new UserImpl();

	public String userLogin(String username, String password) {

		user= new User();
		user.setUsername(username);
		user.setPassword(password);
		return impl.userLogin(user);

	}

	public String adduser(String username, String password) {
		user = new User();
		user.setUsername(username);
		user.setPassword(password);
		return impl.adduser(user);
	}
}
