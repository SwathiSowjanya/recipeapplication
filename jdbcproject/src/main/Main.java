package main;

//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import controller.AdminController;
import controller.ItemController;
import controller.UserController;
//import controller.UserController;
import model.Item;
//import util.Query;

public class Main {
	static String s[];

	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		Scanner scanner = new Scanner(System.in);
		// ItemController controller = new ItemController();
		System.out.println(
				"-*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--");
		System.out.println("\t\t\t\t\tWELCOME to Receipe Application");
		System.out.println("\t\t\t\t\t________________________________");
		System.out.println("1 to Admin || 2 to User");
		System.out.println("Press any option :");
		int selection = scanner.nextInt();

		if (selection == 1) {
			scanner.nextLine();
			AdminController admincontroller = new AdminController();
			System.out.println("Enter username");
			String username = scanner.nextLine();
			System.out.println("Enter password");
			String password = scanner.nextLine();
			// AdminController admincontroller = new AdminController();

			//String result = admincontroller.AdminLogin(username, password);
			System.out.println(admincontroller.AdminLogin(username, password));
			// if (result.equals("Successfully")) {
			System.out.println("_______Admin Page_______");
			ItemController controller = new ItemController();
			try {
				int WouldYouLikeToContinue;
				do {
					System.out.println("MENU: \n 1.Add ITEMS \n 2.View ITEMS \n 3.Update ITEM \n 4.Delete ITEM");
					// System.out.println("Enter your option");
					int Choice = scanner.nextInt();
					switch (Choice) {
					case 1:
						scanner.nextLine();
						System.out.println("Enter ITEM ID, ITEM Name, ITEM Type, ITEM Ingredients for ADD");
						s = scanner.nextLine().split(",");
						System.out.println(controller.addItem(Integer.parseInt(s[0]), s[1], s[2], s[3]));
						break;
					case 2:
						System.out.format("%-20s%-20s%-20s%-20s\n", "ITEM_ID", "ITEM_NAME", "ITEM_Type",
								"ITEM_Ingredients");
						for (Item i : controller.viewItem()) {
							System.out.println(i);
						}
						break;

					case 3:
						scanner.nextLine();
						System.out.println("Enter Item ID ITEM Name, ITEM Type, ITEM Ingredients  for UPDATE");
						s = scanner.nextLine().split(",");
						System.out.println(controller.updateItem(Integer.parseInt(s[0]), s[1], s[2], s[3]));
						break;

					case 4:
						System.out.println("Enter Item ID for DELETE");
						Integer id = scanner.nextInt();
						System.out.println(controller.deleteItem(id));
						break;
					default:
						System.out.println("Please enter valid option");
						System.exit(0);
					}
					System.out.println("\n Do You Want to Continue Operations? if yes-press1,else to Logout press 0");
					WouldYouLikeToContinue = scanner.nextInt();
				} while (WouldYouLikeToContinue == 1);
				System.out.println("*******THANK TOU*******");

			} catch (InputMismatchException ex) {
				System.err.println(ex.getMessage());

			}
		} else if (selection == 2) {
			System.out.println("Press1 for login ,Press 2 for signup");
			int option = scanner.nextInt();
			if (option == 1) {
				scanner.nextLine();
				UserController userController = new UserController();
				System.out.println("Enter username");
				String username = scanner.nextLine();
				System.out.println("Enter password");
				String password = scanner.nextLine();
				System.out.println(userController.userLogin(username, password));
				System.out.println("_______User Page_______");
				ItemController controller = new ItemController();

				try {
					int WouldYouLikeToContinue;
					do {
						System.out.println(" 1.View ITEMS ");
						// System.out.println("Enter your option");
						int Choice = scanner.nextInt();
						switch (Choice) {

						case 1:
							System.out.format("%-20s%-20s%-20s%-20s\n", "ITEM_ID", "ITEM_NAME", "ITEM_Type",
									"ITEM_Ingredients");
							for (Item i : controller.viewItem()) {
								System.out.println(i);
							}
							break;
						default:
							System.out.println("Please enter valid option");
							System.exit(0);
						}
						System.out
								.println("\n Do You Want to Continue Operations? if yes-press1,else to Logout press 0");
						WouldYouLikeToContinue = scanner.nextInt();
					} while (WouldYouLikeToContinue == 1);
					System.out.println("*******THANK TOU*******");

				} catch (InputMismatchException ex) {
					System.err.println(ex.getMessage());
				}

			} else if (option == 2) {
				scanner.nextLine();
				UserController userController = new UserController();
				System.out.println("Enter username");
				String username = scanner.nextLine();
				System.out.println("Enter password");
				String password = scanner.nextLine();
				System.out.println(userController.adduser(username, password));

			} else {
				System.out.println("Please Choose valid option");
			}

		} else {
			System.out.println("username or password is incorrect");

		}
		scanner.close();
	}

}
